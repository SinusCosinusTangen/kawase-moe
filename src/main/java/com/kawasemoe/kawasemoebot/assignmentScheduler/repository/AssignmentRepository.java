package com.kawasemoe.kawasemoebot.assignmentScheduler.repository;

import com.kawasemoe.kawasemoebot.assignmentScheduler.core.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, Integer> {
    Assignment findById(int id);
    Assignment findByAssignment(String assignment);
}
