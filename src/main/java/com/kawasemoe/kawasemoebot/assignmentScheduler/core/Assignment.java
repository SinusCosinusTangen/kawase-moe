package com.kawasemoe.kawasemoebot.assignmentScheduler.core;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "assignment")
@Data
@NoArgsConstructor
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "lecture", updatable = false)
    private String lecture;

    @Column(name = "assignment", columnDefinition = "Text")
    private String assignment;

    @Column(name = "deadline", columnDefinition = "Text")
    private String deadline;

    public Assignment(String lecture, String assignment, String deadline) {
        this.lecture = lecture;
        this.assignment = assignment;
        this.deadline = deadline;
    }
}
