package com.kawasemoe.kawasemoebot.assignmentScheduler.service;

import com.kawasemoe.kawasemoebot.assignmentScheduler.core.Assignment;
import com.kawasemoe.kawasemoebot.assignmentScheduler.repository.AssignmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Locale;

@Service
public class AssignmentService {

    private String lecture;

    @Autowired
    private AssignmentRepository assignmentRepository;

    public AssignmentService(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    public Iterable<Assignment> getAll() {
        return assignmentRepository.findAll();
    }

    public ArrayList<String> getAllAssignment() {
        int id = 0;

        ArrayList<Integer> idDb = new ArrayList<>();
        for (Assignment assignment:getAll()) {
            idDb.add(assignment.getId());
        }

        ArrayList<String> assignmentList = new ArrayList<>();
        assignmentList.add("List tugas saat ini");
        for (Assignment assignment:getAll()) {
            id++;
            String compiledText = "";
            compiledText += id + ". ";
            compiledText += assignmentRepository.findById((int) idDb.get(id - 1)).getLecture();
            compiledText += " | ";
            compiledText += assignmentRepository.findById((int) idDb.get(id - 1)).getAssignment();
            compiledText += " | ";
            compiledText += assignmentRepository.findById((int) idDb.get(id - 1)).getDeadline();
            assignmentList.add(compiledText);
        }
        return assignmentList;
    }

    public ArrayList<String> getCompiledAssignment(String lecture) {
        int id = 0;

        ArrayList<Integer> idDb = new ArrayList<>();
        for (Assignment assignment:getAll()) {
            idDb.add(assignment.getId());
        }

        int no = 1;
        ArrayList<String> assignmentList = new ArrayList<>();
        assignmentList.add("List tugas mata kuliah " + lecture);
        for (Assignment assignment:getAll()) {
            id++;
            String compiledText = "";
            if (assignment.getLecture().toLowerCase().equals(lecture.toLowerCase())) {
                compiledText += no + ". ";
                compiledText += assignmentRepository.findById((int) idDb.get(id - 1)).getAssignment();
                compiledText += " | ";
                compiledText += assignmentRepository.findById((int) idDb.get(id - 1)).getDeadline();
                assignmentList.add(compiledText);
                no++;
            }
        }
        return assignmentList;
    }

    public String createAssignment(String lecture, String assignment, String deadline) {
        assignmentRepository.save(new Assignment(lecture, assignment, deadline));
        return "Berhasil menambahkan tugas " + assignment;
    }

    public String deleteAssignment(String assignment) {
        assignmentRepository.delete(assignmentRepository.findByAssignment(assignment));
        return "Berhasil menghapus tugas " + assignment;
    }
}
