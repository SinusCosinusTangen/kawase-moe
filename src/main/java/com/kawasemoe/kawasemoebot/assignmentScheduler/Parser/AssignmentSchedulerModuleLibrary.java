package com.kawasemoe.kawasemoebot.assignmentScheduler.Parser;

import com.kawasemoe.kawasemoebot.assignmentScheduler.service.AssignmentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssignmentSchedulerModuleLibrary {

    private static AssignmentService assignmentService;

    @Autowired
    public void setServices(AssignmentService assignmentService){
        AssignmentSchedulerModuleLibrary.assignmentService = assignmentService;
    }

    /**
     * Consumes 2 arguments
     * <ul>
     * <li>{@code String title}</li>
     * <li>{@code String content}</li>
     * </ul>
     */
    public static final Module GET_ALL_ASSIGNMENT = new Module() {
        @Override
        public Object produce() {
            return assignmentService.getAllAssignment();
        }

        @Override
        int params() {return 0;}
    };

    public static final Module GET_COMPILED_ASSIGNMENT= new Module() {
        @Override
        public Object produce() {
            return assignmentService.getCompiledAssignment(args.get(0));
        }

        @Override
        int params() {return 1;}
    };

    public static final Module CREATE_ASSIGNMENT= new Module() {
        @Override
        public Object produce() {
            return assignmentService.createAssignment(args.get(0), args.get(1), args.get(2));
        }

        @Override
        int params() {return 3;}
    };

    public static final Module DELETE_ASSIGNMENT= new Module() {
        @Override
        public Object produce() {
            return assignmentService.deleteAssignment(args.get(0));
        }

        @Override
        int params() {return 1;}
    };
}
