package com.kawasemoe.kawasemoebot.assignmentScheduler.Parser;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.ArrayList;

class ReplyList {

    private ArrayList<String> ret = null;

    public ReplyList(ArrayList<String> ret) {
        this.ret = ret;
    }

    public ArrayList<String> get() {
        return ret;
    }
}