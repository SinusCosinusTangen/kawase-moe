package com.kawasemoe.kawasemoebot.assignmentScheduler.Parser;

import static com.kawasemoe.kawasemoebot.assignmentScheduler.Parser.AssignmentSchedulerModuleLibrary.*;

import java.util.ArrayList;
import java.util.Map;

public class CommandParser {

    private static final Map<String, Module> MAP = Map.of(
            "getAllAssignment", GET_ALL_ASSIGNMENT,
            "getCompiledAssignment", GET_COMPILED_ASSIGNMENT,
            "createAssignment", CREATE_ASSIGNMENT,
            "deleteAssignment", DELETE_ASSIGNMENT
    );

    public static ArrayList<String> getAllAssignment() {
        Module module = MAP.get("getAllAssignment").create();
        Object result = module.invoke();
        return consume2(result);
    }

    public static ArrayList<String> getCompiledAssignment(String lecture) {
        Module module = MAP.get("getCompiledAssignment").create();
        try {
            module.addArgs2(lecture);
        } catch (Exception e) {

        }
        Object result = module.invoke();
        return consume2(result);
    }

    public static String createAssignment(String[] args) {
        Module module = MAP.get("createAssignment").create();
        try {
            module.addArgs(args);
        } catch (Exception e) {

        }
        Object result = module.invoke();
        return consume(result);
    }

    public static String deleteAssignment(String args) {
        Module module = MAP.get("deleteAssignment").create();
        try {
            module.addArgs2(args);
        } catch (Exception e) {

        }
        Object result = module.invoke();
        return consume(result);
    }

    private static String consume(Object ret) {
        Reply reply = new Reply((String) ret);
        return reply.get();
    }

    private static ArrayList<String> consume2(Object ret) {
        ReplyList reply = new ReplyList((ArrayList<String>) ret);
        return reply.get();
    }
}
