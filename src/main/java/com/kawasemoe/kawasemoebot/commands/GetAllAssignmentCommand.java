package com.kawasemoe.kawasemoebot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.kawasemoe.kawasemoebot.assignmentScheduler.Parser.CommandParser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class GetAllAssignmentCommand extends Command {

    public GetAllAssignmentCommand() {
        this.name = "list";
        this.help = "List tugas";
    }

    @Override
    protected void execute(CommandEvent event) {
        ArrayList<String> assignment = CommandParser.getAllAssignment();
        for (String reply:assignment) {
            event.reply(reply);
        }
    }
}
