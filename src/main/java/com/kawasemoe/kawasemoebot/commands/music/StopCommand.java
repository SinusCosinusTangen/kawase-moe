package com.kawasemoe.kawasemoebot.commands.music;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.kawasemoe.kawasemoebot.lavaplayer.GuildMusicManager;
import com.kawasemoe.kawasemoebot.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

public class StopCommand extends Command {

    public StopCommand() {
        this.name = "stop";
        this.help = "Stops playing music and clears queue";
    }

    @Override
    protected void execute(CommandEvent event) {
        final TextChannel channel = (TextChannel) event.getChannel();
        final Member self = event.getSelfMember();
        final GuildVoiceState selfVoiceState = self.getVoiceState();

        if(!selfVoiceState.inVoiceChannel()) {
            channel.sendMessage("I need to be in a voice channel!").queue();
            return;
        }

        final Member member = event.getMember();
        final GuildVoiceState memberVoiceState = member.getVoiceState();

        if (!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("You need to be in a voice channel!").queue();
            return;
        }

        if (!memberVoiceState.getChannel().equals(selfVoiceState.getChannel())) {
            channel.sendMessage("You need to be in the same voice channel as me!").queue();
            return;
        }

        final GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(event.getGuild());
        musicManager.schedule.player.stopTrack();
        musicManager.schedule.queue.clear();
        channel.sendMessage("The player has been stopped and queue is clear!").queue();
    }
}
