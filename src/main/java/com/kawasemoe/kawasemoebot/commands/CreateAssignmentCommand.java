package com.kawasemoe.kawasemoebot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.kawasemoe.kawasemoebot.assignmentScheduler.Parser.CommandParser;

public class CreateAssignmentCommand extends Command {

    public CreateAssignmentCommand() {
        this.name = "add";
        this.help = "Tambahkan tugas \"matkul\" ;;; \"Tugasnya\" ;;; \"Deadline\"";
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split(" ;;; ");
        String assignment = CommandParser.createAssignment(args);
        event.reply(assignment);
    }
}
