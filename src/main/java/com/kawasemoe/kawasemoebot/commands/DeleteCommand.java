package com.kawasemoe.kawasemoebot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.kawasemoe.kawasemoebot.assignmentScheduler.Parser.CommandParser;

public class DeleteCommand extends Command {

    public DeleteCommand() {
        this.name = "delete";
        this.help = "Hapus tugas \"Tugas\"";
    }

    @Override
    protected void execute(CommandEvent event) {
        String args = event.getArgs();
        String assignment = CommandParser.deleteAssignment(args);
        event.reply(assignment);
    }
}