package com.kawasemoe.kawasemoebot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.kawasemoe.kawasemoebot.assignmentScheduler.Parser.CommandParser;

import java.util.ArrayList;

public class GetAssignmentCommand extends Command {

    public GetAssignmentCommand() {
        this.name = "tugas";
        this.help = "Tugas spesifik mata kuliah";
    }

    @Override
    protected void execute(CommandEvent event) {
        String args = event.getArgs();
        ArrayList<String> assignment = CommandParser.getCompiledAssignment(args.toLowerCase());
        if (assignment.size() == 0) {
            event.reply("Tidak ada tugas untuk mata kuliah " + args);
        }
        for (String reply:assignment) {
            event.reply(reply);
        }
    }
}
